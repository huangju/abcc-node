var express = require('express');
var app = express();
var router = express.Router();

router.use("/gateway", require('./mock/index'))
router.use("/common",require('./mock/common'))
app.use("/api/v1", router)

app.get('/', (req, res) => {
    res.send('Hello World!');
})

app.listen(3000);
console.log('start 3000')