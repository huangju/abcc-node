const Mock = require("mockjs")
const express = require("express")
const router = express.Router();
const _ = require("lodash");
const { randomStr, randomArrayMap } = require('./helper');

const other = {
    "errCode": "0",
    "errStr": "success",
    "ret": "0"
}

router.use("/Home/GetDeals", function (req, res) {
    res.send({
        "head": {
            "method": "market.deals",
            "msgType": "response",
            "packType": "1",
            "lang": "cn",
            "version": "1.0.0",
            "timestamps": "1530884374",
            "serialNumber": "57",
            "userId": "1",
            "userToken": "56"
        },
        "data": [{
            "id": _.random(101, 200),
            "time": "任意值",
            "fillPrice": randomStr(1000, 10000),
            "fillQuantity": randomStr(10000, 20000),
            "side": ["2", '1'][_.random(0, 1)]
        }].concat(randomArrayMap(10).map((item, index) => ({
            "id": _.random(101, 200),
            "time": "任意值",
            "fillPrice": randomStr(1000, 10000),
            "fillQuantity": randomStr(10000, 20000),
            "side": ["2", '1'][_.random(0, 1)]
        }))),
        ...other
    })
})


module.exports = router;