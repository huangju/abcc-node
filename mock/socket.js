import { Server } from 'mock-socket'

class MockServer {
    constructor(url, INTERVAL = 5000) {
        this.interval = null
        this.subScribes = []
        this.server = new Server(url)
        this.server.on('connection', socket => {
            clearTimeout(this.interval)
            this.socket = socket
            // 收到message
            socket.on('message', (e) => {
                // 返回数据
                if (this.onMessage) this.onMessage(e)
            })
            socket.on('close', (e) => {
                this.subScribes = []
                if (this.onClose) this.onClose()
            })

            // 定时发送信息
            this.interval = setInterval(() => {
                this.subScribes.forEach((item = {}) => {
                    if (_.isFunction(item.func)) {
                        item.func()
                    }
                })
            }, INTERVAL)
        })
    }

    sendJson = (obj) => {
        if (this.socket) {
            this.socket.send(JSON.stringify(obj))
        }
    }

    subScribe = (obj) => {
        // 如果对象有name&func就会push进subScribes数组
        if (_.has(obj, 'name') && _.has(obj, 'func')) {
            _.remove(this.subScribes, item => item.name === obj.name)
            this.subScribes.push(obj)
        } else {
            console.log('订阅的对象必须包含name属性和func属性')
        }
    }

    clearStacks = () => {
        this.subScribes = []
    }
}

const mockServer2 = new MockServer();

mockServer2.onMessage = (e) => {
    if (!e) return
    const message = JSON.parse(e);
    const { head: { method } } = message;
    if (method === 'deals.subscribe') {
        mockServer2.subScribe({
            name: 'deals.update',
            func: () => {
                mockServer2.sendJson(
                    {
                        "method": "deals.update",
                        "timestamp": 1535000397026,
                        "data": [{
                            "id": `${_.random(1000, 10000)}`,
                            "time": "12:59:56",
                            "fillPrice": `${_.random(10, 100)}`,
                            "fillQuantity": "1",
                            "side": "1"
                        }]
                    }
                )
            }
        })
    }
}