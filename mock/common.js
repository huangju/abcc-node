const express = require("express")
const router = express.Router();
const _ = require("lodash");

router.use('/home', (request, response) => {
    response.send({
        data: {
            banners: [{
                imgUrl: "https://i.loli.net/2018/12/06/5c08a92419f1f.png",
                link: "",
            }],
            notifies: [{
                title: "www.hopex.com正式上线",
                link: "https://www.hopex.com/about?page=AboutUs"
            }],
            top3: [{
                "allowTrade": true,
                "contractCode": "BTCUSD",
                "contractName": "BTC/USD永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-3704.0",
                "lastestPriceUSD": "$3704.00",
                "changePercent24h": "-3.65%",
                "sumAmount24h": "1,129 BTC"
            }, {
                "allowTrade": true,
                "contractCode": "BTCUSDT",
                "contractName": "BTC/USDT永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-3750.0",
                "lastestPriceUSD": "$3689.63",
                "changePercent24h": "-3.43%",
                "sumAmount24h": "3,547,018 USDT"
            }, {
                "allowTrade": true,
                "contractCode": "ETHUSDT",
                "contractName": "ETH/USDT永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-101.15",
                "lastestPriceUSD": "$99.52",
                "changePercent24h": "-5.60%",
                "sumAmount24h": "2,008,805 USDT"
            }],
            top5: [{
                "allowTrade": true,
                "contractCode": "XRPUSDT",
                "contractName": "XRP/USDT永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-0.3368",
                "lastestPriceUSD": "$0.33",
                "changePercent24h": "-2.63%",
                "sumAmount24h": "196,293 USDT"
            }, {
                "allowTrade": true,
                "contractCode": "BTCUSDT",
                "contractName": "BTC/USDT永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-3750.0",
                "lastestPriceUSD": "$3689.63",
                "changePercent24h": "-3.43%",
                "sumAmount24h": "3,547,018 USDT"
            }, {
                "allowTrade": true,
                "contractCode": "BTCUSD",
                "contractName": "BTC/USD永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-3704.0",
                "lastestPriceUSD": "$3704.00",
                "changePercent24h": "-3.65%",
                "sumAmount24h": "1,129 BTC"
            }, {
                "allowTrade": true,
                "contractCode": "LTCUSDT",
                "contractName": "LTC/USDT永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-28.89",
                "lastestPriceUSD": "$28.42",
                "changePercent24h": "-4.84%",
                "sumAmount24h": "146,785 USDT"
            }, {
                "allowTrade": true,
                "contractCode": "ETHUSDT",
                "contractName": "ETH/USDT永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-101.15",
                "lastestPriceUSD": "$99.52",
                "changePercent24h": "-5.60%",
                "sumAmount24h": "2,008,805 USDT"
            }],
            last5: [{
                "allowTrade": true,
                "contractCode": "ETHUSD",
                "contractName": "ETH/USD永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-99.85",
                "lastestPriceUSD": "$99.85",
                "changePercent24h": "-5.85%",
                "sumAmount24h": "12,769 ETH"
            }, {
                "allowTrade": true,
                "contractCode": "ETHUSDT",
                "contractName": "ETH/USDT永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-101.15",
                "lastestPriceUSD": "$99.52",
                "changePercent24h": "-5.60%",
                "sumAmount24h": "2,008,805 USDT"
            }, {
                "allowTrade": true,
                "contractCode": "LTCUSDT",
                "contractName": "LTC/USDT永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-28.89",
                "lastestPriceUSD": "$28.42",
                "changePercent24h": "-4.84%",
                "sumAmount24h": "146,785 USDT"
            }, {
                "allowTrade": true,
                "contractCode": "BTCUSD",
                "contractName": "BTC/USD永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-3704.0",
                "lastestPriceUSD": "$3704.00",
                "changePercent24h": "-3.65%",
                "sumAmount24h": "1,129 BTC"
            }, {
                "allowTrade": true,
                "contractCode": "BTCUSDT",
                "contractName": "BTC/USDT永续",
                "hasPosition": false,
                "posiDirect": 0,
                "posiDirectD": "",
                "lastestPrice": "-3750.0",
                "lastestPriceUSD": "$3689.63",
                "changePercent24h": "-3.43%",
                "sumAmount24h": "3,547,018 USDT"
            }],
        },
        ret: 0,
        errCode: "",
        errStr: "",
        env: 0,
        timestamp: 1544065372574
    });
})


module.exports = router;